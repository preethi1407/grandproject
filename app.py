from flask import Flask,jsonify,render_template, request, session,redirect,flash,url_for
from flask_mysqldb import MySQL
from functools import wraps
import MySQLdb.cursors
import re

# connecting to the database
app = Flask(__name__)

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'Preethi2000'
app.config['MYSQL_DB'] = 'profile'


mysql = MySQL(app)


# login
@app.route('/login', methods =['GET', 'POST'])
# this function is used to check the credentials and login or else it throws an error
def login():
	msg=''
	if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
		username = request.form['username']
		password = request.form['password']
		cursor = mysql.connection.cursor()
		cursor.execute(f'SELECT * FROM register WHERE username = "{username}" AND password = "{password}"')
		account = cursor.fetchone()
		if account:
			session['logged_in'] = True
			return render_template('home.html')
		else:
			msg = {
			"msg" : 'incorrect username/password',
            "username" : "null",
            "password" : "null",
			"status": "inactive"
        	}
	return render_template('login.html',msg=msg)

def login_required(test):
# this function is used for authentication purpose 
	@wraps(test)
	def wrap(*args,**kwargs):
		if 'logged_in' in session:
			return test(*args,**kwargs)
		else:
			flash('invalid credentials Please Login again')
			return redirect(url_for('login'))
	return wrap



# register
@app.route('/register', methods =['GET', 'POST'])
# this function is used to create a new profile and is used to check if an account already
def register():
	msg = ''
	if request.method == 'POST' and 'username' in request.form and 'password' in request.form and 'email' in request.form and 'dateofbirth' in request.form  and 'country' in request.form :
		username = request.form['username']
		password = request.form['password']
		email = request.form['email']
		dateofbirth = request.form['dateofbirth']
		country = request.form['country']
		cursor = mysql.connection.cursor()
		cursor.execute(f'SELECT * FROM register WHERE username ="{username}"')
		account = cursor.fetchone()
		if account:
			data = {
			"msg" : 'Account already exists !',
			}
			return jsonify(data)
		else:
			cursor.execute('INSERT INTO register VALUES (%s, %s, %s, %s, %s)', (username, dateofbirth, email, password,country))
			mysql.connection.commit()
			data = {
			"msg" : 'You have successfully registered !',
			"username": username,
      "email" : email,
      "password" : password,
			"date of birth": dateofbirth,
			"country":country,
			"status": "active"
        	}
			return jsonify(data)
		
	elif request.method == 'POST':
		msg = 'Please fill out the form !'
	return render_template('register.html', msg =msg)

# home
@app.route("/home")
@login_required
def home():
# this function is used to redirect to "home.html" page if valid credentials are given or else it returns back to the login page
	return render_template('home.html')
	

# sports
@app.route("/sports")
@login_required
def sports():
# this function is used to redirect to "sports.html" page if valid credentials are given or else it returns back to the login page
	if session['logged_in'] == True:
		return render_template('sports.html')
	return redirect(url_for('login'))
	
# culture
@app.route("/culture")
@login_required
def culture():
# this function is used to redirect to "culture.html" page if valid credentials are given or else it returns back to the login page
	if session['logged_in'] == True:
		return render_template('culture.html')	
	return redirect(url_for('login'))
	
# tv
@app.route("/tv")
@login_required
def tv():
# this function is used to redirect to "tv.html" page if valid credentials are given or else it returns back to the login page
	if session['logged_in'] == True:
		return render_template('tv.html')
	return redirect(url_for('login'))

# weather 
@app.route("/weather")
@login_required
def weather():
# this function is used to redirect to "weather.html" page if valid credentials are given or else it returns back to the login page
	if session['logged_in'] == True:
		return render_template('weather.html')	
	return redirect(url_for('login'))

# sounds   
@app.route("/sounds")
@login_required
def sounds():
# this function is used to redirect to "sounds.html" page if valid credentials are given or else it returns back to the login page
	if session['logged_in'] == True:
		return render_template('sounds.html')
	return redirect(url_for('login'))

# logout
@app.route("/logout")
def logout():
# this function is used to logout of the logged-in profile
	session.clear()
	flash('You are now logged out','success')
	return redirect(url_for('login'))

#login-taking the parameters(username,password) in the url(path parameters)
@app.route('/new_login/<string:username>/<string:password>')
def new_login(username,password):
# this function is used to check the credentials and login or else it throws an error
	cursor = mysql.connection.cursor()  #url:http://127.0.0.1:8001/new_login/preethi/preethi
	cursor.execute(f'SELECT * FROM register WHERE username = "{username}" AND password = "{password}"')
	account = cursor.fetchone()
	if account:                                          
		session['logged_in'] = True
		return render_template('home.html')
	return render_template('login.html')


#login- query parameters
@app.route('/new_login_query')
def new_login_query():
# this function is used to check the credentials and login or else it throws an error
	username=request.args['username']
	password=request.args['password']
	cursor = mysql.connection.cursor() 
	cursor.execute(f'SELECT * FROM register WHERE username = "{username}" AND password = "{password}"')
	account = cursor.fetchone()
	if account:                                 #url:http://127.0.0.1:8001/new_login_query?username=gopi&password=gopi         
		session['logged_in'] = True
		return render_template('home.html')
	return render_template('login.html')

# register-using path parameters
@app.route('/registerpath/<string:username>/<string:password>/<string:email>/<string:dateofbirth>/<string:country>')
def path_register(username,password,email,dateofbirth,country):
# this function is used to create a new profile and is used to check if an account already
	cursor = mysql.connection.cursor()
	cursor.execute(f'SELECT * FROM register WHERE username ="{username}"')
	account = cursor.fetchone()
	if account:
		data = {
		"msg" : 'Account already exists !',
		}
		return jsonify(data)
	else:
		cursor.execute('INSERT INTO register VALUES (%s, %s, %s, %s, %s)', (username, dateofbirth, email, password,country))
		mysql.connection.commit()
		data = {
		"msg" : 'You have successfully registered !',
		"username": username,
		"email" : email,
		"password" : password,
		"date of birth": dateofbirth,
		"country":country,
		"status": "active"
				}
		return jsonify(data)

# register-using query parameters
@app.route('/registerquery')
def query_register():
# this function is used to create a new profile and is used to check if an account already
	username=request.args['username']
	password=request.args['password']
	email=request.args['email']
	dateofbirth=request.args['dateofbirth']
	country=request.args['country']
	cursor = mysql.connection.cursor()
	cursor.execute(f'SELECT * FROM register WHERE username ="{username}"')
	account = cursor.fetchone()
	if account:
		data = {
		"msg" : 'Account already exists !',
		}
		return jsonify(data)
	else:
		cursor.execute('INSERT INTO register VALUES (%s, %s, %s, %s, %s)', (username, dateofbirth, email, password,country))
		mysql.connection.commit()
		data = {
		"msg" : 'You have successfully registered !',
		"username": username,
		"email" : email,
		"password" : password,
		"date of birth": dateofbirth,
		"country":country,
		"status": "active"
				}
		return jsonify(data)

# login using postman
@app.route("/login_postman",methods=["POST"])
def login_postman():
# this function is used to check the credentials and login or else it throws an error
	data=request.json
	username=data['username']
	password=data['password']
	cursor = mysql.connection.cursor() 
	cursor.execute(f'SELECT * FROM register WHERE username = "{username}" AND password = "{password}"')
	account = cursor.fetchone()
	if account:                                        
		session['logged_in'] = True
		return render_template('home.html')
	return render_template('login.html')

# register-using postman
@app.route('/register_postman',methods=["POST"])
def register_postman():
# this function is used to create a new profile and is used to check if an account already
	data=request.json
	username=data['username']
	password=data['password']
	email=data['email']
	dateofbirth=data['dateofbirth']
	country=data['country']
	cursor = mysql.connection.cursor()
	cursor.execute(f'SELECT * FROM register WHERE username ="{username}"')
	account = cursor.fetchone()
	if account:
		data = {
		"msg" : 'Account already exists !',
		}
		return jsonify(data)
	else:
		cursor.execute('INSERT INTO register VALUES (%s, %s, %s, %s, %s)', (username, dateofbirth, email, password,country))
		mysql.connection.commit()
		data = {
		"msg" : 'You have successfully registered !',
		"username": username,
		"email" : email,
		"password" : password,
		"date of birth": dateofbirth,
		"country":country,
		"status": "active"
				}
		return jsonify(data)


if __name__ == "__main__":
	app.secret_key='secret123'
	app.run(debug=True,port=8001)

