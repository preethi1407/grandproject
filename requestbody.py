from datetime import date
from typing import Optional
from flask_mysqldb import MySQL
from flask_sugar import Sugar,jsonify
from pydantic import BaseModel


app = Sugar(__name__)

# connecting to the database
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'Preethi2000'
app.config['MYSQL_DB'] = 'profile'


mysql = MySQL(app)

# register request body schema
class register(BaseModel):
    username: str
    password: str
    email: str
    dateofbirth: date
    country:str


@app.post("/register/")
def create(register: register):
# this function is used to add a new user to the database 
    username=register.username
    password=register.password
    email=register.email
    dateofbirth=register.dateofbirth
    country=register.country
    cursor = mysql.connection.cursor()
    cursor.execute(f'SELECT * FROM register WHERE username ="{username}"')
    account = cursor.fetchone()
    if account:
        data = {
        "msg" : 'Account already exists !',
        }
        return jsonify(data)
    else:
        cursor.execute('INSERT INTO register VALUES (%s, %s, %s, %s, %s)', (username, dateofbirth, email, password,country))
        mysql.connection.commit()
        data = {
        "msg" : 'You have successfully registered !',
        "username": username,
        "email" : email,
        "password" : password,
        "date of birth": dateofbirth,
        "country":country,
        "status": "active"
        }
        return jsonify(data)

if __name__ == "__main__":
    app.run()